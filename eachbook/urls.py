from django.contrib import admin
from django.urls import path
from eachbook.views import show_books, create_view, show_book, update_view, delete_view
#from eachbook.views import mags_list, mags_form


urlpatterns = [
    path("", show_books, name="show_books"),
    path("form/", create_view, name="form"),
    path("<int:pk>/", show_book, name="showbook"), # detail view
    path("<int:pk>/edit/", update_view, name="edit_book"),
    path("<int:pk>/delete/", delete_view, name="delete_book"),
    
    # path("mags/", mags_list, name="mags_list"),
    # path("mags/form/", mags_form, name="mags_form"),
    # path("<int:pk>/", mags_create, name="mags_create"),

]

