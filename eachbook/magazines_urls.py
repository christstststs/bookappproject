#from django.contrib import admin
from django.urls import path
from eachbook.views import mags_list, mags_detail, mags_create, mags_update, mags_delete

urlpatterns = [
    path("", mags_list, name="mags_list"),
    path("<int:pk>/", mags_detail, name="mags_detail"),
    path("form/", mags_create, name="mags_create"),
    path("<int:pk>/update/", mags_update, name="mags_update"), #update
    path("<int:pk>/delete/", mags_delete, name="mags_delete"),

]