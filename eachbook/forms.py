#from dataclasses import field
from django import forms
from .models import Book
from .models import Magazine

# Creating a form 
#try:
class BookForm(forms.ModelForm):
    class Meta:
        model = Book

        fields = ["title", "author", "number_of_pages", 
        "ISBN", "in_print", "cover",]
#except Exception:
    pass

class MagForm(forms.ModelForm):
    class Meta:
        model = Magazine
        #exclude = []
        fields = ["title", "release_cycle", "description", 
        "cover_image",]
    pass
