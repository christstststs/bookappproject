from django.shortcuts import redirect, render, get_object_or_404
from eachbook.models import Book, Magazine
from .forms import BookForm, MagForm



def show_books(request):
    books = Book.objects.all()
    context = {
        "books":books
    }
    return render(request, "books\list.html", context) 
    # whatever it return, will send back to the client

    # any keys on the context direction that you pass in in the render function, 
    # its available in the template as a variable

def create_view(request):
    # dictionary for initial data with 
    # field names as keys
    context ={}

    # add the dictionary during initialization
    form = BookForm(request.POST or None)
    if form.is_valid():    #if this form is fill out, if it is true
        form.save()        #save it to the form
        return redirect("show_books")

    context['form']= form
    return render(request, "books/form.html", context)
    # what do I need to produce a page

    # context: it provde the variable inside the templete
    # function always has input and output: you want
    # to get the output


## create a view here       
def show_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
    "book": book
    }
    return render(request, "books/detail.html", context)

#update view
def update_view(request, pk): 
    book = get_object_or_404(Book, pk = pk)
    form = BookForm(request.POST or None, instance = book)
    if form.is_valid():
        book=form.save()
        return redirect("showbook", pk=book.pk)

    context ={}
    context['form']= form
    return render(request, "books/update.html", context)

#delete view
def delete_view(request, pk):
    book = get_object_or_404(Book, pk = pk)
    
    if request.method =="POST":
        # delete object
        book.delete()
        return redirect("show_books")
        
    context ={}
    return render(request, "books/delete.html", context)

####magazine###
# mags_list
def mags_list(request):
    mags = Magazine.objects.all()
    context = {
        "mags":mags
    }
    return render(request, "magazines/list.html", context)

# create view
# mags_form
def mags_create(request):
    context ={}

    # add the dictionary during initialization
    form = MagForm(request.POST or None)
    if form.is_valid():    #if this form is fill out, if it is true
        form.save()        #save it to the form
        return redirect("mags_list")

    context['form']= form
    return render(request, "magazines/form.html", context)


#mags_detail
def mags_detail(request, pk):
    mags = Magazine.objects.get(pk=pk)
    context = {
    "mags": mags
    }
    return render(request, "magazines/detail.html", context)


#mags_update
def mags_update(request, pk): 
    mags = get_object_or_404(Magazine, pk = pk)
    form = MagForm(request.POST or None, instance = mags)
    if form.is_valid():
        mags=form.save()
        return redirect("mags_list")

    context ={}
    context['form']= form
    return render(request, "magazines/update.html", context)

#mags_delete
def mags_delete(request, pk):
    mags = get_object_or_404(Magazine, pk = pk)
    
    if request.method =="POST":
        # delete object
        mags.delete()
        return redirect("mags_list")
        
    context ={}
    return render(request, "magazines/delete.html", context)
