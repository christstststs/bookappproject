from django.contrib import admin
from eachbook.models import Author, Book, BookReview, Magazine
# Register your models here.

admin.site.register(Book)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Magazine)

