from django.apps import AppConfig


class EachbookConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'eachbook'
