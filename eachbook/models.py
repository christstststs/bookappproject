from pyexpat import model
from turtle import title
from django.db import models

# Create your models here.
# class Book(models.Model):

#     title = models.CharField(max_length=125)
#     author = models.CharField(max_length=100)
#     number_of_pages = models.SmallIntegerField(null=True)
#     ISBN = models.BigIntegerField(null=True)
#     cover = models.URLField(null=True, blank=True)
#     in_print = models.BooleanField(null=True)
#     published_date = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return self.title + " by " + self.author

###
class  Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField("Author", related_name="books", blank=True)
    number_of_pages = models.SmallIntegerField(null=True)
    ISBN = models.BigIntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)
    published_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name



class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()
    
    # This function is method that if you want to 
    # repreesnt the instance of the models, return it as string.
    #In Django, On the admin page, we use this to determine what to display


# Create a new magazine model
class Magazine(models.Model):

    title = models.CharField(max_length=125)
    release_cycle = models.TextField(null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.title





